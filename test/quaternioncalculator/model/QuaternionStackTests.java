/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quaternioncalculator.model;

import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author luiz
 */
public class QuaternionStackTests {

    @Test
    public void stackTest() {
        NumberStack<Quaternion> stack;
        stack = new NumberStack<>();
        stack.push(new Quaternion(0, 1, 0, 0));
        stack.push(new Quaternion(0, 0, 1, 0));
        stack.swap();
        Assert.assertEquals(new Quaternion(0, 1, 0, 0), stack.pop());
        Assert.assertEquals(new Quaternion(0, 0, 1, 0), stack.pop());
        Assert.assertTrue(stack.isEmpty());
    }

}