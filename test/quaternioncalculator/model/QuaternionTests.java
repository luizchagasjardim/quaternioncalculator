/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quaternioncalculator.model;

import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author luiz
 */
public class QuaternionTests {

    Quaternion q;
    Quaternion p;

    @Test
    public void contructorsAndToStringTests() {
        q = new Quaternion(1, 3, 6, 3);
        Assert.assertEquals("1.0 + 3.0i + 6.0j + 3.0k", q.toString());
        q = new Quaternion(5);
        Assert.assertEquals("5.0", q.toString());
        q = new Quaternion(7, -1, -2);
        Assert.assertEquals("7.0i - 1.0j - 2.0k", q.toString());
        q = new Quaternion();
        Assert.assertEquals("0.0", q.toString());
        q = new Quaternion(0, -1, 0, 1);
        Assert.assertEquals("-1.0i + 1.0k", q.toString());
    }

    @Test
    public void regexTest() {
        q = new Quaternion("0.0 + 1.0 i - 1.0 j + 0.0 k");
        Assert.assertEquals("1.0i - 1.0j", q.toString());
        Assert.assertEquals(q.toString(), (new Quaternion(q.toString())).toString());
        q = new Quaternion("12.89 i - 1");
        Assert.assertEquals("-1.0 + 12.89i", q.toString());
        q = new Quaternion("i");
        Assert.assertEquals("1.0i", q.toString());
        q = new Quaternion("");
        Assert.assertEquals("0.0", q.toString());
        q = new Quaternion("i + 2i");
        Assert.assertEquals("3.0i", q.toString());
        q = new Quaternion("i + j");
        Assert.assertEquals("1.0i + 1.0j", q.toString());
    }

    @Test
    public void partsTests() {
        q = new Quaternion(3, 0, 1, 2);
        //parts
        Assert.assertEquals(new Quaternion(3, 0, 0, 0), q.realPart());
        Assert.assertEquals(new Quaternion(0, 0, 1, 2), q.vectorPart());
        Assert.assertEquals(new Quaternion(0, 0, 0, 0), q.iPart());
        Assert.assertEquals(new Quaternion(0, 0, 1, 0), q.jPart());
        Assert.assertEquals(new Quaternion(0, 0, 0, 2), q.kPart());
        //is
        Assert.assertTrue(q.realPart().isReal());
        Assert.assertFalse(q.isReal());
        Assert.assertTrue(q.vectorPart().isVector());
        Assert.assertFalse(q.isVector());
    }

    @Test
    public void conjugateTests() {
        q = new Quaternion(3, -4, 0, 12);
        Assert.assertEquals(new Quaternion(13, 0, 0, 0), q.modulus());
        Assert.assertEquals(new Quaternion(3, 4, 0, -12), q.conjugate());
        Assert.assertTrue(q.modulus().times(q.modulus())
                .equals(q.times(q.conjugate())));
        Assert.assertTrue(q.realPart().times(new Quaternion(2))
                .equals(q.plus(q.conjugate())));
        Assert.assertEquals(new Quaternion(1, 0, 0, 0), q.times(q.inverse()));
        Assert.assertEquals(q.times(new Quaternion(-1)), q.negative());
    }

    @Test
    public void plusMinusTests() {
        q = new Quaternion(1, -1, 0, 1);
        p = new Quaternion(0, 2, -3, 0);
        Assert.assertEquals(q.plus(p), p.plus(q));
        Assert.assertEquals(new Quaternion(1, 1, -3, 1), q.plus(p));
        Assert.assertEquals(p.minus(q), q.minus(p).negative());
        Assert.assertEquals(p.minus(q), p.plus(q.negative()));
        Assert.assertEquals(new Quaternion(1, -3, 3, 1), q.minus(p));
    }

    @Test
    public void timesDividedByTests() {
        //TODO
    }

    @Test
    public void conjugatedByTests() {
        //TODO
    }

    @Test
    public void dotCrossTests() {
        //TODO
    }

}