/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quaternioncalculator.view;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import quaternioncalculator.model.NumberStack;
import quaternioncalculator.model.Quaternion;

/**
 * FXML Controller class
 *
 * @author luiz
 */
public class QuaternionCalculatorController implements Initializable {

    @FXML
    private TextField input;

    @FXML
    private ListView output;

    @FXML
    private ListView stack;

    private final NumberStack<Quaternion> quaternionStack = new NumberStack<>();
    private final ObservableList outputObservableList = FXCollections.observableArrayList();
    private final ObservableList stackObservableList = FXCollections.observableArrayList();
    private final Quaternion[] memory = new Quaternion[10];

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        outputObservableList.addListener((ListChangeListener.Change change) -> {
            output.setItems(outputObservableList);
            output.scrollTo(outputObservableList.size());
        });

        stackObservableList.addListener((ListChangeListener.Change change) -> {
            stack.setItems(stackObservableList);
            stack.scrollTo(stackObservableList.size());
        });

        /*
            KEYBOARD SHORTCUTS
		enter - enter

		(ctrl+)
		clear screen - backspace
		r - r
		x - x
		y - y
		z - z
		vec - v
		real?
		vector?
		|q| - m
		bar(q) - b
		q^(-1) - i
		-q - n
                load - num

		(ctrl+shift+)
		q+p - p
		q-p - m
		q*p - t
		q/p - d
		q . p - .
		q x p - x
		p q p^(-1) - c
                save - num

		(alt+)
		delete - del
		swap - s
		clear stack - backspace
         */
        input.setOnKeyPressed((KeyEvent e) -> {
            if (e.isControlDown() && !e.isAltDown()) {
                if (e.isShiftDown()) {
                    switch (e.getCode()) {
                        case P:
                            plus();
                            break;
                        case M:
                            minus();
                            break;
                        case T:
                            times();
                            break;
                        case D:
                            dividedBy();
                            break;
                        case PERIOD:
                            dot();
                            break;
                        case X:
                            cross();
                            break;
                        case C:
                            conjugatedBy();
                            break;
                        case DIGIT1:
                            save(1);
                            break;
                        case DIGIT2:
                            save(2);
                            break;
                        case DIGIT3:
                            save(3);
                            break;
                        case DIGIT4:
                            save(4);
                            break;
                        case DIGIT5:
                            save(5);
                            break;
                        case DIGIT6:
                            save(6);
                            break;
                        case DIGIT7:
                            save(7);
                            break;
                        case DIGIT8:
                            save(8);
                            break;
                        case DIGIT9:
                            save(9);
                            break;
                        case DIGIT0:
                            save(0);
                            break;
                    }
                } else {
                    switch (e.getCode()) {
                        case BACK_SPACE:
                            clear();
                            break;
                        case R:
                            realPart();
                            break;
                        case X:
                            iPart();
                            break;
                        case Y:
                            jPart();
                            break;
                        case Z:
                            kPart();
                            break;
                        case C:
                            vectorPart();
                            break;
                        case M:
                            modulus();
                            break;
                        case B:
                            conjugate();
                            break;
                        case I:
                            inverse();
                            break;
                        case N:
                            negative();
                            break;
                        case DIGIT1:
                            load(1);
                            break;
                        case DIGIT2:
                            load(2);
                            break;
                        case DIGIT3:
                            load(3);
                            break;
                        case DIGIT4:
                            load(4);
                            break;
                        case DIGIT5:
                            load(5);
                            break;
                        case DIGIT6:
                            load(6);
                            break;
                        case DIGIT7:
                            load(7);
                            break;
                        case DIGIT8:
                            load(8);
                            break;
                        case DIGIT9:
                            load(9);
                            break;
                        case DIGIT0:
                            load(0);
                            break;
                    }
                }
            } else if (e.isAltDown() && !e.isControlDown()) {
                switch (e.getCode()) {
                    case DELETE:
                        deleteFromStack();
                        break;
                    case S:
                        swapTopTwoOnStack();
                        break;
                    case BACK_SPACE:
                        clearStack();
                        break;
                }
            } else if (!e.isAltDown() && !e.isControlDown()) {
                switch (e.getCode()) {
                    case ENTER:
                        enter();
                        break;
                }
            }
        });

    }

    private void readQ() {
        try {
            Quaternion q = new Quaternion(input.getText());
            quaternionStack.push(q);
            print("Entering into the stack: " + q.toString());
            input.clear();
        } catch (IllegalArgumentException e) {
            print("Error reading input");
        }
        printStack();
    }

    @FXML
    private void enter() {
        readQ();
    }

    @FXML
    private void clear() {
        outputObservableList.clear();
    }

    @FXML
    private void deleteFromStack() {
        quaternionStack.pop();
        printStack();
    }

    @FXML
    private void swapTopTwoOnStack() {
        try {
            quaternionStack.swap();
        } catch (RuntimeException e) {
            print(e.getMessage());
        }
        printStack();
    }

    @FXML
    private void clearStack() {
        quaternionStack.clear();
        printStack();
    }

    private void print(String str) {
        outputObservableList.add(str);
    }

    private void printStack() {
        stackObservableList.clear();
        stackObservableList.addAll(quaternionStack);
    }

    private Quaternion getQ() {
        if (!input.getText().isEmpty()) {
            readQ();
        }
        return quaternionStack.isEmpty() ? new Quaternion() : quaternionStack.pop();
    }

    @FXML
    private void realPart() {
        String str = "Re(";
        Quaternion q = getQ();
        str += q.toString() + ") = ";
        q = q.realPart();
        str += q.toString();
        quaternionStack.push(q);
        print(str);
        printStack();
    }

    @FXML
    private void iPart() {
        String str = "I(";
        Quaternion q = getQ();
        str += q.toString() + ") = ";
        q = q.iPart();
        str += q.toString();
        quaternionStack.push(q);
        print(str);
        printStack();
    }

    @FXML
    private void jPart() {
        String str = "J(";
        Quaternion q = getQ();
        str += q.toString() + ") = ";
        q = q.jPart();
        str += q.toString();
        quaternionStack.push(q);
        print(str);
        printStack();
    }

    @FXML
    private void kPart() {
        String str = "K(";
        Quaternion q = getQ();
        str += q.toString() + ") = ";
        q = q.kPart();
        str += q.toString();
        quaternionStack.push(q);
        print(str);
        printStack();
    }

    @FXML
    private void vectorPart() {
        String str = "Vec(";
        Quaternion q = getQ();
        str += q.toString() + ") = ";
        q = q.realPart();
        str += q.toString();
        quaternionStack.push(q);
        print(str);
        printStack();
    }

    @FXML
    private void isReal() {
        if (!input.getText().isEmpty()) {
            readQ();
        }
        Quaternion q = quaternionStack.isEmpty() ? new Quaternion() : quaternionStack.peek();
        print(q.toString() + " is " + (q.isReal() ? "" : "not ") + "a real number");
        printStack();
    }

    @FXML
    private void isVector() {
        if (!input.getText().isEmpty()) {
            readQ();
        }
        Quaternion q = quaternionStack.isEmpty() ? new Quaternion() : quaternionStack.peek();
        print(q.toString() + " is " + (q.isVector() ? "" : "not ") + "a pure vector");
        printStack();
    }

    @FXML
    private void modulus() {
        String str = "|";
        Quaternion q = getQ();
        str += q.toString() + "| = ";
        q = q.modulus();
        str += q.toString();
        quaternionStack.push(q);
        print(str);
        printStack();
    }

    @FXML
    private void conjugate() {
        try {
            String str = "bar(";
            Quaternion q = getQ();
            str += q.toString() + ") = ";
            q = q.conjugate();
            str += q.toString();
            quaternionStack.push(q);
            print(str);
            printStack();
        } catch (IllegalArgumentException e) {
            print(e.getMessage());
        }
    }

    @FXML
    private void inverse() {
        try {
            Quaternion q = getQ();
            String str = q.toString() + "^(-1) = ";
            q = q.inverse();
            str += q.toString();
            quaternionStack.push(q);
            print(str);
            printStack();
        } catch (IllegalArgumentException e) {
            print(e.getMessage());
        }
    }

    @FXML
    private void negative() {
        String str = "-(";
        Quaternion q = getQ();
        str += q.toString() + ") = ";
        q = q.negative();
        str += q.toString();
        quaternionStack.push(q);
        print(str);
        printStack();
    }

    @FXML
    private void plus() {
        Quaternion q = getQ();
        Quaternion p = getQ();
        String str = "(" + p.toString() + ") + (" + q.toString() + ") = ";
        q = p.plus(q);
        str += q.toString();
        quaternionStack.push(q);
        print(str);
        printStack();
    }

    @FXML
    private void minus() {
        Quaternion q = getQ();
        Quaternion p = getQ();
        String str = "(" + p.toString() + ") - (" + q.toString() + ") = ";
        q = p.minus(q);
        str += q.toString();
        quaternionStack.push(q);
        print(str);
        printStack();
    }

    @FXML
    private void times() {
        Quaternion q = getQ();
        Quaternion p = getQ();
        String str = "(" + p.toString() + ") * (" + q.toString() + ") = ";
        q = p.times(q);
        str += q.toString();
        quaternionStack.push(q);
        print(str);
        printStack();
    }

    @FXML
    private void dividedBy() {
        try {
            Quaternion q = getQ();
            Quaternion p = getQ();
            String str = "(" + p.toString() + ") / (" + q.toString() + ") = ";
            q = p.dividedBy(q);
            str += q.toString();
            quaternionStack.push(q);
            print(str);
            printStack();
        } catch (IllegalArgumentException e) {
            print(e.getMessage());
        }
    }

    @FXML
    private void conjugatedBy() {
        try {
            Quaternion q = getQ();
            Quaternion p = getQ();
            String str = "(" + p.toString() + ") * (" + q.toString() + ") * (" + p.toString() + ")^(-1) = ";
            q = p.conjugatedBy(q);
            str += q.toString();
            quaternionStack.push(q);
            print(str);
            printStack();
        } catch (IllegalArgumentException e) {
            print(e.getMessage());
        }
    }

    @FXML
    private void dot() {
        try {
            Quaternion q = getQ();
            Quaternion p = getQ();
            String str = "(" + p.toString() + ") . (" + q.toString() + ") = ";
            q = p.dot(q);
            str += q.toString();
            quaternionStack.push(q);
            print(str);
            printStack();
        } catch (IllegalArgumentException e) {
            print(e.getMessage());
        }
    }

    @FXML
    private void cross() {
        try {
            Quaternion q = getQ();
            Quaternion p = getQ();
            String str = "(" + p.toString() + ") x (" + q.toString() + ") = ";
            q = p.cross(q);
            str += q.toString();
            quaternionStack.push(q);
            print(str);
            printStack();
        } catch (IllegalArgumentException e) {
            print(e.getMessage());
        }
    }

    private void save(int i) {
        memory[i] = getQ();
        print("Saved " + i + ": " + memory[i]);
        printStack();
    }

    private void load(int i) {
        if (memory[i] != null) {
            quaternionStack.push(memory[i]);
            print("Loaded " + i + ": " + memory[i]);
            printStack();
        } else {
            print("There is no number saved at " + i + ".");
        }
    }
}
