/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quaternioncalculator.model;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Represents a quaternion q = r + xi + yj + zk.
 *
 * @author luiz
 */
public final class Quaternion extends Number<Quaternion> {

    private static final String PART = "[+-]?\\d*(\\.\\d*)?[ijk]?";
    private static final Pattern PART_PATTERN = Pattern.compile(PART);

    private final double r;
    private final double x;
    private final double y;
    private final double z;

    public Quaternion(double r, double x, double y, double z) {
        this.r = r;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Quaternion(double r) {
        this.r = r;
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }

    public Quaternion(double x, double y, double z) {
        this.r = 0;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Quaternion() {
        this.r = 0;
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }

    public Quaternion(String str) throws IllegalArgumentException{
        str = str.replaceAll("\\s*", "");
        if (!str.matches("(" + PART + ")*")) {
            throw new IllegalArgumentException("String inválido!");
        }
        Matcher matcher = PART_PATTERN.matcher(str);
        double rPart = 0;
        double xPart = 0;
        double yPart = 0;
        double zPart = 0;
        while (matcher.find()) {
            String match = matcher.group();
            if (match.isEmpty()) {
                continue;
            }
            if (match.endsWith("i")) {
                if (match.matches("\\+*i")) {
                    xPart += 1;
                } else if (match.matches("-i")) {
                    xPart -= 1;
                } else {
                    xPart += Double.parseDouble(match.split("i")[0]);
                }
            } else if (match.endsWith("j")) {
                if (match.matches("\\+*j")) {
                    yPart += 1;
                } else if (match.matches("-j")) {
                    yPart -= 1;
                } else {
                    yPart += Double.parseDouble(match.split("j")[0]);
                }
            } else if (match.endsWith("k")) {
                if (match.matches("\\+*k")) {
                    zPart += 1;
                } else if (match.matches("-k")) {
                    zPart -= 1;
                } else {
                    zPart += Double.parseDouble(match.split("k")[0]);
                }
            } else {
                rPart += Double.parseDouble(match);
            }
        }
        this.r = rPart;
        this.x = xPart;
        this.y = yPart;
        this.z = zPart;
    }

    public Quaternion realPart() {
        return new Quaternion(this.r, 0, 0, 0);
    }

    public Quaternion vectorPart() {
        return new Quaternion(0, this.x, this.y, this.z);
    }

    public Quaternion iPart() {
        return new Quaternion(0, this.x, 0, 0);
    }

    public Quaternion jPart() {
        return new Quaternion(0, 0, this.y, 0);
    }

    public Quaternion kPart() {
        return new Quaternion(0, 0, 0, this.z);
    }

    public boolean isReal() {
        return this.x == 0 && this.y == 0 && this.z == 0;
    }

    public boolean isVector() {
        return this.r == 0;
    }

    private double realModulusSquared() {
        return this.r * this.r + this.x * this.x + this.y * this.y + this.z * this.z;
    }

    @Override
    public Quaternion modulus() {
        return new Quaternion(Math.sqrt(this.realModulusSquared()));
    }

    @Override
    public Quaternion conjugate() {
        return new Quaternion(this.r, -this.x, -this.y, -this.z);
    }

    @Override
    public Quaternion inverse() {
        double m = this.realModulusSquared();
        if (m == 0) {
            throw new IllegalArgumentException("0 has no inverse");
        }
        return this.conjugate().times(1 / m);
    }

    @Override
    public Quaternion negative() {
        return new Quaternion(-this.r, -this.x, -this.y, -this.z);
    }

    @Override
    public Quaternion plus(Quaternion q) {
        return new Quaternion(this.r + q.r, this.x + q.x, this.y + q.y, this.z + q.z);
    }

    @Override
    public Quaternion minus(Quaternion q) {
        return this.plus(q.negative());
    }

    @Override
    public Quaternion times(Quaternion q) {
        return new Quaternion(this.r * q.r - this.x * q.x - this.y * q.y - this.z * q.z,
                this.r * q.x + this.x * q.r + this.y * q.z - this.z * q.y,
                this.r * q.y - this.x * q.z + this.y * q.r + this.z * q.x,
                this.r * q.z + this.x * q.y - this.y * q.x + this.z * q.r);
    }

    private Quaternion times(double d) {
        return new Quaternion(d * this.r, d * this.x, d * this.y, d * this.z);
    }

    @Override
    public Quaternion dividedBy(Quaternion q) {
        return this.times(q.inverse());
    }

    @Override
    public Quaternion conjugatedBy(Quaternion q) {
        return q.times(this.times(q.inverse()));
    }

    public Quaternion dot(Quaternion q) {
        if (this.isVector()) {
            if (q.isVector()) {
                return new Quaternion(this.x * q.x + this.y * q.y + this.z * q.z);
            }
        }
        throw new IllegalArgumentException("One of the factors in the dot product is not a pure vector.");
    }

    public Quaternion cross(Quaternion q) {
        if (this.isVector()) {
            if (q.isVector()) {
                return new Quaternion(this.y * q.z - this.z * q.y, this.z * q.x - this.x * q.z, this.x * q.y - this.y * q.x);
            }
        }
        throw new IllegalArgumentException("One of the factors in the cross product is not a pure vector.");
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Quaternion) {
            Quaternion v = (Quaternion) o;
            return this.r == v.r && this.x == v.x && this.y == v.y && this.z == v.z;
        }
        return false;
    }

    @Override
    public String toString() {
        double x = this.x;
        double y = this.y;
        double z = this.z;
        String str = "";
        if (this.r != 0) {
            str += this.r;
        }
        if (x != 0) {
            if (str.isEmpty()) {
                str += x;
            } else {
                str += x > 0 ? " + " + x : " - " + (-x);
            }
            str += "i";
        }
        if (y != 0) {
            if (str.isEmpty()) {
                str += y;
            } else {
                str += y > 0 ? " + " + y : " - " + (-y);
            }
            str += "j";
        }
        if (z != 0) {
            if (str.isEmpty()) {
                str += z;
            } else {
                str += z > 0 ? " + " + z : " - " + (-z);
            }
            str += "k";
        }
        return str.isEmpty() ? "0.0" : str;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.r, this.x, this.y, this.z);
    }
}