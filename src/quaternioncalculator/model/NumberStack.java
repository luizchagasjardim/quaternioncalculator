/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quaternioncalculator.model;

import java.util.Stack;

/**
 * Number Stack
 * 
 * @author luiz
 * @param <T> Type of Number
 */
public class NumberStack<T extends Number<T>> extends Stack<T> {

    public void swap() {
        try {
            T t = this.pop();
            T u = this.pop();
            this.push(t);
            this.push(u);
        } catch (RuntimeException e) {
            throw new RuntimeException("Stack has less than two items.");
        } 
    }

}