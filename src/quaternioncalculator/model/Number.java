/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quaternioncalculator.model;

/**
 * Abstract class of numbers.
 * Many of the abstract methods could be implemented, e.g., minus(T t) could be
 * implemented as "return this.plus(t.negative);" but I would rather implement it
 * every time so it's optimized and doesn't construct more objects than necessary.
 * 
 * @author luiz
 * @param <T> Type of number, which must extend from this class
 */
public abstract class Number<T extends Number> {

    /**
     * Returns the modulus of the number as a new instance of the class.
     * 
     * @return |this|
     */
    public abstract T modulus();

    /**
     * Return the conjugate of the number as a new instance of the class.
     * The conjugate should be such that x.plus(x.conjugate()) and
     * x.times(x.conjugate()) are both real numbers.
     * 
     * @return conjugate
     */
    public abstract T conjugate();

    /**
     * Returns the multiplicative inverse of the number as a new instance of the class.
     * The inverse should be such that x.times(x.inverse()) is the 
     * real number 1.
     * 
     * @return this^(-1)
     */
    public abstract T inverse();

    /**
     * Returns the number multiplied by -1 as a new instance of the class.
     * 
     * @return -this
     */
    public abstract T negative();

    /**
     * Returns the sum of this and t as a new instance of the class.
     * 
     * @param t
     * @return this + t in this order
     */
    public abstract T plus(T t);

    /**
     * Returns the subtraction of this by t as a new instance of the class.
     * 
     * @param t
     * @return this - t
     */
    public abstract T minus(T t);

    /**
     * Returns the product of this by t as a new instance of the class.
     * 
     * @param t
     * @return this * t
     */
    public abstract T times(T t);

    /**
     * Returns the division of this by t as a new instance of the class.
     * 
     * @param t
     * @return this / t
     */
    public abstract T dividedBy(T t);

    /**
     * Returns the conjugation of this by t as a new instance of the class.
     * 
     * @param t
     * @return t * this * t^(-1)
     */
    public abstract T conjugatedBy(T t);
    
    /**
     * A reminder to override equals.
     * Throws an exception when called.
     * 
     * @param o
     * @return never returns
     */
    @Override
    public boolean equals(Object o) {
        throw new RuntimeException("The equals method should be overriden when extending from Number");
    }

    /**
     * A reminder to override hashCode.
     * Throws an exception when called.
     * 
     * @return never returns
     */
    @Override
    public int hashCode() {
        throw new RuntimeException("The hashCode method should be overriden when extending from Number");
    }

    /**
     * A reminder to override toString.
     * Throws an exception when called.
     * 
     * @return never returns
     */
    @Override
    public String toString() {
        throw new RuntimeException("The toString method should be overriden when extending from Number");
    }
}